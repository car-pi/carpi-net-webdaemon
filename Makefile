# Define default script
.DEFAULT_GOAL := test
# Use Bash as runtime
SHELL := /bin/bash

# Project Files
SOLUTION := src/CarPi.Net/CarPi.Net.sln
CSPROJ_DATA_TRANSFORMER := src/CarPi.Net/CarPi.Net.Worker.DataTransformer/CarPi.Net.Worker.DataTransformer.csproj
CSPROJ_SIMULATOR := src/CarPi.Net/CarPi.Net.Tools.Simulator/CarPi.Net.Tools.Simulator.csproj
CSPROJ_WEBDAEMON := src/CarPi.Net/CarPi.Net.WebDaemon/CarPi.Net.WebDaemon.csproj
CSPROJ_REPORTER := src/CarPi.Net/CarPi.Net.Worker.Reporter/CarPi.Net.Worker.Reporter.csproj
TEST_ARTIFACTS := src/CarPi.Net/artifacts

# Package Names
PKG_DATA_TRANSFORMER := carpi-data-transformer
PKG_SIMULATOR := carpi-simulator
PKG_WEBDAEMON := carpi-webdaemon
PKG_REPORTER := carpi-reporter

# Output Dirs
OUT_DIR := out
OUT_INSTALL_DIR := /opt/
OUT_SIMULATOR := $(OUT_DIR)/$(PKG_SIMULATOR)

# Constants for build parameters
PROJ_ARC := xplat
RUNTIME_ARC_ARM := arm
RUNTIME_ARC_ARM64 := arm64
RUNTIME_ARC_X64 := x64
DEB_ARC_ARM := armhf
DEB_ARC_X64 := amd64


print-version:
	echo "Version to be built is $(PACKAGE_VERSION)"

clean:
	dotnet clean $(SOLUTION)
	rm -rf $(CURDIR)/$(OUT_DIR)
	rm -rf $(CURDIR)/$(TEST_ARTIFACTS)

restore:
	dotnet restore $(SOLUTION)

build: restore
	dotnet build $(SOLUTION)

test: build
	dotnet test $(SOLUTION) \
		--test-adapter-path:. --logger:"junit;LogFilePath=../artifacts/{assembly}-test-result.xml;MethodFormat=Class;FailureBodyFormat=Verbose"

build-simulator-x64: build
	$(SHELL) $(CURDIR)/scripts/build-deb.sh \
		$(CSPROJ_SIMULATOR) \
		$(PKG_SIMULATOR) \
		$(RUNTIME_ARC_X64) \
		$(DEB_ARC_X64)

build-simulator-arm: build
	$(SHELL) $(CURDIR)/scripts/build-deb.sh \
		$(CSPROJ_SIMULATOR) \
		$(PKG_SIMULATOR) \
		$(RUNTIME_ARC_ARM) \
		$(DEB_ARC_ARM)

build-webdaemon-x64: build
	$(SHELL) $(CURDIR)/scripts/build-deb.sh \
		$(CSPROJ_WEBDAEMON) \
		$(PKG_WEBDAEMON) \
		$(RUNTIME_ARC_X64) \
		$(DEB_ARC_X64)

build-webdaemon-arm: build
	$(SHELL) $(CURDIR)/scripts/build-deb.sh \
		$(CSPROJ_WEBDAEMON) \
		$(PKG_WEBDAEMON) \
		$(RUNTIME_ARC_ARM) \
		$(DEB_ARC_ARM)

build-data-transformer-x64: build
	$(SHELL) $(CURDIR)/scripts/build-deb.sh \
		$(CSPROJ_DATA_TRANSFORMER) \
		$(PKG_DATA_TRANSFORMER) \
		$(RUNTIME_ARC_X64) \
		$(DEB_ARC_X64)

build-data-transformer-arm: build
	$(SHELL) $(CURDIR)/scripts/build-deb.sh \
		$(CSPROJ_DATA_TRANSFORMER) \
		$(PKG_DATA_TRANSFORMER) \
		$(RUNTIME_ARC_ARM) \
		$(DEB_ARC_ARM)

build-reporter-x64: build
	$(SHELL) $(CURDIR)/scripts/build-deb.sh \
		$(CSPROJ_REPORTER) \
		$(PKG_REPORTER) \
		$(RUNTIME_ARC_X64) \
		$(DEB_ARC_X64)

build-reporter-arm: build
	$(SHELL) $(CURDIR)/scripts/build-deb.sh \
		$(CSPROJ_REPORTER) \
		$(PKG_REPORTER) \
		$(RUNTIME_ARC_ARM) \
		$(DEB_ARC_ARM)

build-simulator: build-simulator-arm build-simulator-x64
build-webdaemon: build-webdaemon-arm build-webdaemon-x64
build-data-transformer: build-data-transformer-arm build-data-transformer-x64
build-reporter: build-reporter-arm build-reporter-x64

ci-build: build-simulator build-webdaemon build-data-transformer build-reporter