#!/usr/bin/env sh
source "scripts/release_vars.sh"

echo "Uploading Xplat artifact to ${URL_XPLAT} ..."
curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
  --upload-file "${SRC_XPLAT}" "${URL_XPLAT}"
