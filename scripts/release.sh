#!/usr/bin/env sh
source "scripts/release_vars.sh"

release-cli create \
  --name "Release $PACKAGE_VERSION" \
  --tag-name "${CI_COMMIT_TAG}" \
  --assets-link "{\"name\":\"Cross-platform release (requires .NET 5 runtime)\",\"url\":\"${URL_XPLAT}\"}" #\
#  --assets-link "{\"name\":\"Windows release\",\"url\":\"${URL_WIN}\"}" \
#  --assets-link "{\"name\":\"Linux (x64) release\",\"url\":\"${URL_LNX}\"}"
