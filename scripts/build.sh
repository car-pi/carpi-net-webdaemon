#!/usr/bin/env sh
PROJECT=${1}
PROJECT_ARC=${2}
RUNTIME_ARC=${3}
OUTPUT_PATH=${4:=publish/${PROJECT_ARC}_${RUNTIME_ARC}/}
PACKAGE_VERSION=${PACKAGE_VERSION}

echo "Build Parameters:"
echo "  Project:              ${PROJECT}"
echo "  Project Architecture: ${PROJECT_ARC}"
echo "  Runtime Architecture: ${RUNTIME_ARC}"
echo "  Output Path:          ${OUTPUT_PATH}"
echo "  Package Version:      ${PACKAGE_VERSION}"

if [ ! -d "${OUTPUT_PATH}" ]; then
  echo "Creating output directory ${OUTPUT_PATH}"
  mkdir -p "${OUTPUT_PATH}"
fi

if [ "$PROJECT_ARC" = "xplat" ]; then
  dotnet publish \
    -c Release \
    -a "${RUNTIME_ARC}" \
    -o "${OUTPUT_PATH}" \
    -p:DebugType=None \
    /p:Version="${PACKAGE_VERSION}" \
    /p:InformationalVersion="${PACKAGE_VERSION}-${CI_COMMIT_SHORT_SHA}" \
    $PROJECT
else
  dotnet publish \
    -c Release \
    -a "${RUNTIME_ARC}" \
    -o "${OUTPUT_PATH}" \
    --self-contained \
    --runtime "${PROJECT_ARC}" \
    -p:PublishSingleFile=true \
    -p:DebugType=None \
    /p:Version="${PACKAGE_VERSION}" \
    /p:InformationalVersion="${PACKAGE_VERSION}-${CI_COMMIT_SHORT_SHA}" \
    $PROJECT
fi

#cd "${OUTPUT_PATH}" && zip -r "../../carpi-net-webdaemon_${PROJECT_ARC}_${RUNTIME_ARC}.zip" .
#cd ../../
