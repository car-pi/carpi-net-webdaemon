#!/usr/bin/env bash
set -e

FORMAT_BOLD="$(tput bold)"
FORMAT_RESET="$(tput sgr0)"
COLOR_DEFAULT="$(tput setaf 12)"
COLOR_SUCCESS="$(tput setaf 10)"
COLOR_WARN="$(tput setaf 9)"
COLOR_RESET="$(tput op)"

function log() {
    echo "${COLOR_DEFAULT} > ${FORMAT_BOLD}${1}${FORMAT_RESET}${COLOR_RESET}"
}
function logp() {
    echo "${COLOR_DEFAULT}   ${FORMAT_BOLD}${1}${FORMAT_RESET}${COLOR_RESET}"
}
function logs() {
    echo "${COLOR_SUCCESS} ✔️ ${FORMAT_BOLD}${1}${FORMAT_RESET}${COLOR_RESET}"
}
function loge() {
    echo "${COLOR_WARN} > ${FORMAT_BOLD}${1}${FORMAT_RESET}${COLOR_RESET}"
}

# --- Settings --------------------------------------------
TEMP_FILE=$(mktemp -u)
EXTRACT_DIR=$(mktemp -d)
URL="https://gitlab.com/car-pi/carpi-net-webdaemon/-/jobs/artifacts/master/download?job=build-release-deb"

# --- Start -----------------------------------------------
if [[ "$EUID" = 0 ]]; then
    logs "You are root, continuing ..."
else
    log "You are not root, requesting sudo password ..."
    sudo -k
    if sudo true; then
        logs "sudo access confirmed"
    else
        loge "Failed to grant sudo access"
        exit 1
    fi
fi

log "Downloading latest artifacts to ${TEMP_FILE} ..."
wget -O "${TEMP_FILE}" ${URL}

log "Extracting archive to ${EXTRACT_DIR}"
unzip "${TEMP_FILE}" "out/arm/*.deb" -d "${EXTRACT_DIR}"

deb_packages=$(find "${EXTRACT_DIR}" -iname '*.deb')
deb_count=$(echo "${deb_packages}" | wc -l)
log "Installing ${deb_count} packages ..."

for file in `echo "${deb_packages}"`; do
    logp "Installing $(basename $file) ..."
    sudo dpkg -i "${file}"
done

logs "Installed ${deb_count} packages"
log "Cleaning up ..."
rm "${TEMP_FILE}"
rm -rf "${EXTRACT_DIR}"
