#!/usr/bin/env sh
export PACKAGE_VERSION=${CI_COMMIT_TAG:=$(date -u +"0.0.0-ci%Y%m%d%H%M")}
PACKAGE_REGISTRY_URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/carpi-net-webdaemon/${PACKAGE_VERSION}

PKG_BASE_NAME=carpi-net-webdaemon

SRC_XPLAT=carpi-net-webdaemon_xplat.zip

PKG_XPLAT=${PKG_BASE_NAME}-xplat-${PACKAGE_VERSION}

URL_XPLAT=${PACKAGE_REGISTRY_URL}/${PKG_XPLAT}.zip
