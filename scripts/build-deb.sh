#!/usr/bin/env sh
set -e

# --- Input Parameters
PROJECT=${1}
PACKAGE_NAME=${2}
RUNTIME_ARC=${3}
DEB_RUNTIME_ARC=${4:=${RUNTIME_ARC}}

PACKAGE_VERSION=${PACKAGE_VERSION}

# --- Functions
function ensure_dir() {
    if [ ! -d "${1}" ]; then
        echo "Creating output directory ${1}"
        mkdir -p "${1}"
    fi
}

# --- STARTUP HEADER
cat << EOF
=====================================
= BUILDING DEBIAN PACKAGE FOR CARPI =
=====================================
= Project: ${PROJECT}
= Package: ${PACKAGE_NAME}
= Runtime: ${RUNTIME_ARC} (${DEB_RUNTIME_ARC})
= Version: ${PACKAGE_VERSION}
EOF

# --- Prepare variables
# - Global
export DEB_RUNTIME_ARC=${DEB_RUNTIME_ARC}
# - DEB packages
deb_file="${PACKAGE_NAME}-${RUNTIME_ARC}-${PACKAGE_VERSION}.deb"
deb_src="deb/${PACKAGE_NAME}"
deb_path="out/${PACKAGE_NAME}-${RUNTIME_ARC}"
deb_install_path="${deb_path}/opt/${PACKAGE_NAME}"
info_version="${PACKAGE_VERSION}-${CI_COMMIT_SHORT_SHA}"

# --- Run
# - Print out variables
cat << EOF
=== DEB BUILD PARAMETERS ============
= Package to build: ${deb_file}
= DEB source:       ${deb_src}
= Package content:  ${deb_path}
=====================================
EOF

# - Prepare Folders
ensure_dir "${deb_path}/DEBIAN"
ensure_dir "${deb_install_path}"
ensure_dir "out/${RUNTIME_ARC}"

# - Copy DEB skeleton
echo "Copying DEB skeleton from ${deb_src} to ${deb_path} ..."
cp -r ${deb_src}/** "${deb_path}/"
envsubst < "${deb_src}/DEBIAN/control" > "${deb_path}/DEBIAN/control"
chmod -R 755 ${deb_path}/DEBIAN

# - Build Project
echo "Building project ${PROJECT} to ${deb_install_path} ..."
dotnet publish \
    -c Release \
    -a "${RUNTIME_ARC}" \
    -o "${deb_install_path}" \
    -p:DebugType=None \
    /p:Version="${PACKAGE_VERSION}" \
    /p:InformationalVersion="${info_version}" \
    "${PROJECT}"

# - Building DEB package
echo "Building DEB file ${deb_file} from ${deb_path} ..."
dpkg-deb --build "${deb_path}" "out/${RUNTIME_ARC}/${deb_file}"

# - Writing Version out
cat << EOF > out/version.sh
#!/usr/bin/env sh
export PACKAGE_VERSION="${PACKAGE_VERSION}"
EOF

cat << EOF
=====================================
= BUILDING DEBIAN PACKAGE FOR CARPI =
=           ! COMPLETED !           =
=====================================
EOF
