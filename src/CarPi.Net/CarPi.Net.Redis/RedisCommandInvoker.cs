using System.Text.Json;
using CarPi.Net.Core.Command;
using StackExchange.Redis;

namespace CarPi.Net.Redis;

public class RedisCommandInvoker : ICommandInvoker
{
    private readonly ConnectionMultiplexer _redis;

    public RedisCommandInvoker(ConnectionMultiplexer redis)
    {
        _redis = redis;
    }

    public void InvokeCommand(string command)
    {
        InvokeCommand(command, new Dictionary<string, object>());
    }

    public void InvokeCommand(string command, Dictionary<string, object> parameters)
    {
        _redis.GetSubscriber().Publish(
            $"cmd#carpi.{command}",
            JsonSerializer.Serialize(parameters));
    }
}