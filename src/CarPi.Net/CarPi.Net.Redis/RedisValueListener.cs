using CarPi.Net.Core.ValueListener;
using StackExchange.Redis;

namespace CarPi.Net.Redis;

public class RedisValueListener : BaseValueListener
{
    private readonly ConnectionMultiplexer _redis;

    public RedisValueListener(ConnectionMultiplexer redis)
    {
        _redis = redis;
    }

    public override void StartListening()
    {
        _redis.GetSubscriber().Subscribe("*#*", (channel, message) => HandleNewValue(channel, message));
    }

    public override void StopListening()
    {
        _redis.GetSubscriber().UnsubscribeAll();
    }

    public override void Dispose()
    {
        _redis.Dispose();
        base.Dispose();
    }
}