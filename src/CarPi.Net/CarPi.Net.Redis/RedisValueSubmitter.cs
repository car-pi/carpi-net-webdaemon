﻿using CarPi.Net.Core.ValueConversion;
using CarPi.Net.Core.ValueSubmitter;
using StackExchange.Redis;

namespace CarPi.Net.Redis;

public class RedisValueSubmitter : BaseValueSubmitter
{
    private readonly ISubscriber _subscriber;

    public RedisValueSubmitter(
        ConnectionMultiplexer redis,
        IValueConversionManager conversionManager)
        : base(conversionManager)
    {
        _subscriber = redis.GetSubscriber();
    }

    protected override void Submit(string channel, string value)
    {
        _subscriber.Publish(channel, value);
    }
}
