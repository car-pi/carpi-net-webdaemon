using CarPi.Net.Core.Command;
using CarPi.Net.Core.ValueListener;
using CarPi.Net.Core.ValueSubmitter;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;

namespace CarPi.Net.Redis;

public static class Registration
{
    public static IServiceCollection AddRedis(this IServiceCollection services, string endpoint)
    {
        return services
            .AddSingleton<ConfigurationOptions>(r => new ConfigurationOptions
            {
                EndPoints =
                {
                    endpoint
                }
            })
            .AddSingleton<ConnectionMultiplexer>(r => ConnectionMultiplexer
                .Connect(r.GetRequiredService<ConfigurationOptions>()))
            .AddSingleton<IValueListener, RedisValueListener>()
            .AddSingleton<ICommandInvoker, RedisCommandInvoker>()
            .AddSingleton<IValueSubmitter, RedisValueSubmitter>();
    }
}