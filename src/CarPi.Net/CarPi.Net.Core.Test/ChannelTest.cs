using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarPi.Net.Core.Test;

[TestClass]
public class ChannelTest
{
    [DataTestMethod]
    [DataRow("f#carpi.gps.latitude", ValueType.Float, "carpi.gps.latitude")]
    [DataRow("s#carpi.gps.timestamp", ValueType.String, "carpi.gps.timestamp")]
    [DataRow("i#carpi.gps.altitude", ValueType.Integer, "carpi.gps.altitude")]
    [DataRow("b#carpi.can.eco_mode", ValueType.Boolean, "carpi.can.eco_mode")]
    public void ParseChannel(
        string inputChannel,
        ValueType expectedValueType,
        string expectedChannelName)
    {
        var channel = new Channel(inputChannel);
        Assert.AreEqual(expectedValueType, channel.Type);
        Assert.AreEqual(expectedChannelName, channel.Name);
        
        Assert.IsTrue(channel.IsChannel(expectedChannelName));
    }

    [DataTestMethod]
    [DataRow("some_invalid_string", typeof(ArgumentException),
        DisplayName = "Invalid String")]
    [DataRow("q#some_other_type", typeof(KeyNotFoundException),
        DisplayName = "Invalid Value Type")]
    public void ParseInvalidChannel(
        string inputChannel,
        Type expectedExceptionType)
    {
        try
        {
            var _ = new Channel(inputChannel);
            Assert.Fail("No exception was thrown!");
        }
        catch (Exception ex)
        {
            Assert.IsInstanceOfType(ex, expectedExceptionType);
        }
    }

    [DataTestMethod]
    [DataRow("s#carpi.net.ipaddr")]
    [DataRow("s#carpi.net.ipaddr")]
    public void ImplicitConversions(
        string inputChannel)
    {
        Channel ConvertToChannel(string c) => c;
        string ConvertToString(Channel c) => c;
        Channel parsedChannel = ConvertToChannel(inputChannel);
        Assert.IsInstanceOfType(parsedChannel, typeof(Channel));
        Assert.AreEqual(inputChannel, ConvertToString(parsedChannel));
    }
}