using System.Collections.Generic;
using CarPi.Net.Core.ValueConversion;

namespace CarPi.Net.Core.Test.Utils;

internal static class ValueConversionSetup
{
    internal static IValueConversionManager ConstructValueConversionManager()
        => new ValueConversionManager(new List<IValueConverter>
        {
            new StringValueConverter(),
            new BooleanValueConverter(),
            new FloatValueConverter(),
            new IntegerValueConverter()
        });
}