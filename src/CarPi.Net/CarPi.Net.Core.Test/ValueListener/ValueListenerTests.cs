using System;
using System.Collections.Generic;
using CarPi.Net.Core.ValueConversion;
using CarPi.Net.Core.ValueListener;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarPi.Net.Core.Test.ValueListener;

[TestClass]
public class ValueListenerTests
{
    private readonly DummyValueListener _valueListener = new(new ValueConversionManager(new List<IValueConverter>()));

    private void FailureCalled(Exception ex)
    {
        if (ex is not AssertFailedException)
        {
            Assert.Fail("Message Handler Failed was called");
        }
    }

    [TestMethod]
    public void EventsGetCalled()
    {
        var newValueGotCalled = false;
        var channelCallbackGotCalled = false;

        const string CHANNEL = "f#some.float";
        const string MESSAGE = "42.69";

        _valueListener.MessageHandlerFailed += (_, _, e) => FailureCalled(e);
        _valueListener.MessageParsingFailed += (_, _, e) => FailureCalled(e);

        _valueListener.NewValueReceived += value =>
        {
            newValueGotCalled = true;
            Assert.AreEqual(CHANNEL, value.Channel);
            Assert.AreEqual(MESSAGE, value.StringValue);
        };
        _valueListener.RegisterChannelHandler(CHANNEL, value =>
        {
            channelCallbackGotCalled = true;
            Assert.AreEqual(CHANNEL, value.Channel);
            Assert.AreEqual(MESSAGE, value.StringValue);
        });
        
        _valueListener.SubmitValue(CHANNEL, MESSAGE);

        Assert.IsTrue(newValueGotCalled);
        Assert.IsTrue(channelCallbackGotCalled);
    }

    [TestMethod]
    public void ExceptionHandlersGetCalled()
    {
        const string CHANNEL = "f#some.float";
        const string MESSAGE = "invalid!";
        var exToThrow = new Exception("Something happened");

        _valueListener.MessageHandlerFailed += (value, _, e) =>
        {
            Assert.AreEqual(CHANNEL, value.Channel.ToString());
            Assert.AreEqual(MESSAGE, value.StringValue);
            Assert.AreEqual(exToThrow, e);
        };
        _valueListener.MessageParsingFailed += (_, _, _) =>
        {
            Assert.Fail("Message Parsing Failed was called!");
        };

        _valueListener.RegisterChannelHandler(CHANNEL, _ => throw exToThrow);
        _valueListener.SubmitValue(CHANNEL, MESSAGE);
    }
}