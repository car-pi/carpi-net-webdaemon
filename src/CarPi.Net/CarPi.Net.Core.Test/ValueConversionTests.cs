using CarPi.Net.Core.Test.Utils;
using CarPi.Net.Core.ValueConversion;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarPi.Net.Core.Test;

[TestClass]
public class ValueConversionTests
{
    private readonly IValueConversionManager _valueConversionManager
        = ValueConversionSetup.ConstructValueConversionManager();

    [DataTestMethod]
    [DataRow("69", 69f)]
    [DataRow("420.69", 420.69f)]
    [DataRow("1337.42069", 1337.42069f)]
    public void ConvertFloat(string input, float expectedOutput)
    {
        var value = new Value("f#some.test", input);
        Assert.AreEqual(
            expectedOutput,
            _valueConversionManager.ConvertValue<float>(value));
    }

    [DataTestMethod]
    [DataRow("69", 69)]
    [DataRow("420", 420)]
    [DataRow("1337", 1337)]
    public void ConvertInt(string input, int expectedOutput)
    {
        var value = new Value("i#some.test", input);
        Assert.AreEqual(
            expectedOutput,
            _valueConversionManager.ConvertValue<int>(value));
    }

    [DataTestMethod]
    [DataRow("1", true)]
    [DataRow("0", false)]
    [DataRow("something_else", false)]
    public void ConvertBoolean(string input, bool expectedOutput)
    {
        var value = new Value("b#some.test", input);
        Assert.AreEqual(
            expectedOutput,
            _valueConversionManager.ConvertValue<bool>(value));
    }
}