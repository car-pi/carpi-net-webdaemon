﻿namespace CarPi.Net.Core.Keys;

public static class ObdChannels
{
    public const string PREFIX = "carpi.can.";

    public static readonly Channel SysTimestamp = new(ValueType.String, PREFIX, "systimestamp");
    public static readonly Channel ObdVoltage = new(ValueType.Float, PREFIX, "obd_voltage");

    public static readonly Channel AvgBatteryTemp = new(ValueType.Float, PREFIX, "AvgBatteryTemp");
    public static readonly Channel BatterySoC = new(ValueType.Float, PREFIX, "BatterySoC");
    public static readonly Channel BatteryAvailableEnergy = new(ValueType.Float, PREFIX, "BatteryAvailableEnergy");
    public static readonly Channel MaxGeneratedPower = new(ValueType.Float, PREFIX, "MaxGeneratedPower");
    public static readonly Channel MaxAvailablePower = new(ValueType.Float, PREFIX, "MaxAvailablePower");
    public static readonly Channel BatteryVoltage = new(ValueType.Float, PREFIX, "BatteryVoltage");

    public static readonly Channel BatteryCurrent = new(ValueType.Float, PREFIX, "BatteryCurrent");
    public static readonly Channel AvailableChargingPower = new(ValueType.Float, PREFIX, "AvailableChargingPower");
    public static readonly Channel AvailableDischargePower = new(ValueType.Float, PREFIX, "AvailableDischargePower");
    public static readonly Channel EcoMode = new(ValueType.Float, PREFIX, "EcoMode");
    public static readonly Channel EcoModeParsed = new(ValueType.String, PREFIX, "EcoMode.parsed");
    public static readonly Channel VehicleSpeed = new(ValueType.Float, PREFIX, "VehicleSpeed");

    public static readonly Channel VehicleState = new(ValueType.Float, PREFIX, "VehicleState");
    public static readonly Channel VehicleStateParsed = new(ValueType.String, PREFIX, "VehicleState.parsed");

    public static readonly Channel InCarTemperature = new(ValueType.Float, PREFIX, "InCarTemperature");
    public static readonly Channel ExternalTemperature = new(ValueType.Float, PREFIX, "ExternalTemperature");
}
