namespace CarPi.Net.Core.Keys;

public static class ReporterChannels
{
    public const string PREFIX = "carpi.reporter.";

    public static readonly Channel LastSubmittedValue = new(ValueType.String, PREFIX, "last_submitted");
    public static readonly Channel LastSubmittedTimeStamp = new(ValueType.String, PREFIX, "last_submitted_ts");

    public static readonly Channel LastFailureMessage = new(ValueType.String, PREFIX, "last_failure");
    public static readonly Channel LastFailureTimeStamp = new(ValueType.String, PREFIX, "last_failure_ts");
}