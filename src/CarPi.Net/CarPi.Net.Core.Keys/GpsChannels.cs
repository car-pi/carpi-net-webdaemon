﻿namespace CarPi.Net.Core.Keys;

public static class GpsChannels
{
    public const string PREFIX = "carpi.gps.";

    public static readonly Channel Raw = new(ValueType.String, PREFIX, "RAW");
    public static readonly Channel FixMode = new(ValueType.Integer, PREFIX, "fixmode");
    public static readonly Channel Timestamp = new(ValueType.String, PREFIX, "timestamp");
    public static readonly Channel SysTimestamp = new(ValueType.String, PREFIX, "systimestamp");

    public static readonly Channel Latitude = new(ValueType.Float, PREFIX, "latitude");
    public static readonly Channel Longitude = new(ValueType.Float, PREFIX, "longitude");
    public static readonly Channel Altitude = new(ValueType.Integer, PREFIX, "altitude");
    public static readonly Channel Track = new(ValueType.Float, PREFIX, "track");
    public static readonly Channel Climb = new(ValueType.Float, PREFIX, "climb");

    public static readonly Channel Speed = new(ValueType.Float, PREFIX, "speed");
    public static readonly Channel SpeedKph = new(ValueType.Float, PREFIX, "speed.kmh");
    public static readonly Channel SpeedMph = new(ValueType.Float, PREFIX, "speed.mph");

    public static readonly Channel LongitudeError = new(ValueType.Float, PREFIX, "epx");
    public static readonly Channel LatitudeError = new(ValueType.Float, PREFIX, "epy");
    public static readonly Channel AltitudeError = new(ValueType.Float, PREFIX, "epv");

    public static readonly Channel CourseError = new(ValueType.Float, PREFIX, "epd");
    public static readonly Channel SpeedError = new(ValueType.Float, PREFIX, "eps");
    public static readonly Channel ClimbRateError = new(ValueType.Float, PREFIX, "epc");
}
