using CarPi.Net.Core.ValueConversion;
using CarPi.Net.Core.ValueProvider;
using CarPi.Net.Core.ValueTransformation;
using CarPi.Net.Redis;
using CarPi.Net.Worker.DataTransformer;
using CarPi.Net.Worker.DataTransformer.Transformers;
using Serilog;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureAppConfiguration(config => { config.AddYamlFile("/etc/carpi/data-transformer.yaml", optional: true); })
    .UseSerilog((ctx, lc) => lc
        .WriteTo.Console()
        .MinimumLevel.Debug())
    .ConfigureServices((ctx, services) =>
    {
        services
            .AddRedis(ctx.Configuration.GetConnectionString("Redis"))
            .AddSingleton<ValueProvider>()
            .AddValueConversion()
            .RegisterDefaultValueConverter()
            .AddValueTransformer<SpeedValueTransformer>()
            .AddValueTransformer<PowerValueTransformer>()
            .AddHostedService<Worker>();
    })
    .Build();

await host.RunAsync();