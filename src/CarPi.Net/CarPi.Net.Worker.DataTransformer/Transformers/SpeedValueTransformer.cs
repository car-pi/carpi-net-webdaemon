﻿using CarPi.Net.Core;
using CarPi.Net.Core.Keys;
using CarPi.Net.Core.ValueConversion;
using CarPi.Net.Core.ValueSubmitter;
using CarPi.Net.Core.ValueTransformation;

namespace CarPi.Net.Worker.DataTransformer.Transformers;

public class SpeedValueTransformer : BaseValueTransformer
{
    private readonly ILogger<SpeedValueTransformer> _logger;

    public SpeedValueTransformer(ILogger<SpeedValueTransformer> logger, IValueSubmitter valueSubmitter,
        IValueConversionManager conversionManager) : base(valueSubmitter, conversionManager)
    {
        _logger = logger;
    }

    public override Channel[] SubscribedChannels => new[]
    {
        GpsChannels.Speed
    };

    public override void ReceiveValue(Value value)
    {
        float parsedValue = _conversionManager.ConvertValue<float>(value);
        float speedKmh = parsedValue * 3.6f;
        _logger.LogDebug("Got new speed value from {Channel}: {SpeedMs} m/s ({SpeedKmh} km/h)", value.Channel,
            parsedValue, speedKmh);
        _valueSubmitter.SubmitValue("f#custom.gps.speed.kmh", speedKmh);
    }
}