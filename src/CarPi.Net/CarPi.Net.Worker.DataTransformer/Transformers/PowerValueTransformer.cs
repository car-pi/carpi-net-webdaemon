﻿using CarPi.Net.Core;
using CarPi.Net.Core.Keys;
using CarPi.Net.Core.ValueConversion;
using CarPi.Net.Core.ValueSubmitter;
using CarPi.Net.Core.ValueTransformation;

namespace CarPi.Net.Worker.DataTransformer.Transformers;

public class PowerValueTransformer : BaseValueTransformer
{
    private readonly ILogger<PowerValueTransformer> _logger;

    private float? batteryVoltage;
    private float? batteryCurrent;

    public PowerValueTransformer(
        ILogger<PowerValueTransformer> logger,
        IValueSubmitter valueSubmitter,
        IValueConversionManager conversionManager)
        : base(valueSubmitter, conversionManager)
    {
        _logger = logger;
    }

    public override Channel[] SubscribedChannels => new[]
    {
        ObdChannels.BatteryVoltage,
        ObdChannels.BatteryCurrent
    };

    public override void ReceiveValue(Value value)
    {
        _logger.LogDebug("Got new value from {Channel}: {Value}", value.Channel, value.StringValue);
        if (value.Channel == ObdChannels.BatteryVoltage)
        {
            batteryVoltage = _conversionManager.ConvertValue<float>(value);
        }
        else if (value.Channel == ObdChannels.BatteryCurrent)
        {
            batteryCurrent = _conversionManager.ConvertValue<float>(value);
        }
        else
        {
            throw new NotImplementedException($"Channel {value.Channel} is not implemented");
        }

        if (batteryVoltage.HasValue && batteryCurrent.HasValue)
        {
            Process();
        }
    }

    private void Process()
    {
        float powerConsumption = batteryCurrent!.Value * batteryVoltage!.Value / -1000f;
        _logger.LogInformation("Submitting new power consumption value: {PowerConsumption} kW", powerConsumption);
        _valueSubmitter.SubmitValue("f#custom.car.BatteryPower", powerConsumption);
    }
}