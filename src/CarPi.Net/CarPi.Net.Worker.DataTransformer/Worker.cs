using CarPi.Net.Core.ValueConversion;
using CarPi.Net.Core.ValueListener;
using CarPi.Net.Core.ValueSubmitter;
using CarPi.Net.Core.ValueTransformation;

namespace CarPi.Net.Worker.DataTransformer;

public class Worker : BackgroundService
{
    private readonly ILogger<Worker> _logger;
    private readonly IValueListener _valueListener;
    private readonly IValueConversionManager _conversionManager;
    private readonly IValueSubmitter _valueSubmitter;
    private readonly IValueTransformer[] _valueTransformers;

    public Worker(
        ILogger<Worker> logger,
        IValueListener valueListener,
        IValueConversionManager conversionManager,
        IValueSubmitter valueSubmitter,
        IEnumerable<IValueTransformer> valueTransformers)
    {
        _logger = logger;
        _valueListener = valueListener;
        _conversionManager = conversionManager;
        _valueSubmitter = valueSubmitter;
        _valueTransformers = valueTransformers.ToArray();
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        _logger.LogInformation("Starting service ...");
        _valueListener.NewValueReceived += OnNewValueReceived;
        foreach (var transformer in _valueTransformers)
        {
            foreach (var channel in transformer.SubscribedChannels)
            {
                _valueListener.RegisterChannelHandler(channel, transformer.ReceiveValue);
            }
        }

        _valueListener.StartListening();

        while (!stoppingToken.IsCancellationRequested)
        {
            await Task.Delay(1000, stoppingToken);
        }

        _logger.LogInformation("Stopping service ...");
        _valueListener.StopListening();
    }

    private void OnNewValueReceived(Core.Value value)
    {
        _logger.LogTrace("Received value {Key}: {Value}", value.Channel, value.StringValue);
    }
}