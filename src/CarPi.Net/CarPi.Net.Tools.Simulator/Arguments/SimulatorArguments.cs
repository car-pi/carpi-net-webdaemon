using CommandLine;

namespace CarPi.Net.Tools.Simulator.Arguments;

internal class SimulatorArguments
{
    [Value(index: 0, Required = true, MetaName = "RedisAddress",
        HelpText = "IP address and port to Redis instance to send data to")]
    public string RedisAddress { get; set; }

    [Value(index: 1, Required = true, MetaName = "InputFile", HelpText = "Path to file to read in")]
    public IEnumerable<string> InputFiles { get; set; }

    [Option(longName: "mode", shortName: 'm', HelpText = "Mode to use", Default = SimulatorMode.Realtime)]
    public SimulatorMode Mode { get; set; }

    [Option(longName: "interval", shortName: 'i', HelpText = "Interval to use in Constant mode in ms", Default = 100)]
    public int Interval { get; set; }

    [Option(longName: "max-interval", HelpText = "Max amount of ms to sleep for", Default = 10000)]
    public int MaxInterval { get; set; }

    [Option(longName: "loop", HelpText = "Restarts at the beginning of the file when done", Default = false)]
    public bool Loop { get; set; }
}

internal enum SimulatorMode
{
    Realtime,
    Constant
}