using System.Text.Json;
using CarPi.Net.Core;
using CarPi.Net.Tools.Simulator.Arguments;
using Microsoft.Extensions.Logging;

namespace CarPi.Net.Tools.Simulator.Reader;

internal class FileReader
{
    private readonly ILogger<FileReader> _logger;

    public FileReader(ILogger<FileReader> logger)
    {
        _logger = logger;
    }

    public IEnumerable<Record> ReadRecordsFromFile(
        string filePath)
    {
        if (filePath == null) throw new ArgumentNullException(nameof(filePath));
        _logger.LogInformation("Reading records from file {FilePath} ...", filePath);

        using var fs = new FileStream(filePath, FileMode.Open);
        using var sr = new StreamReader(fs);

        uint lineIndex = 0;
        while (!sr.EndOfStream)
        {
            lineIndex++;
            string? line = sr.ReadLine();
            if (line is null)
            {
                _logger.LogWarning("Encountered null-line");
                continue;
            }

            yield return ParseLine(line);
        }
    }

    private static Record ParseLine(string line)
    {
        int firstIndex = line.IndexOf('|');
        int secondIndex = line.IndexOf('|', firstIndex + 1);
        //int thirdIndex = line.IndexOf('|', secondIndex);
        
        string timestamp = line[..firstIndex];
        string channel = line.Substring(firstIndex + 1, secondIndex - firstIndex - 1);
        string value = line[(secondIndex + 1)..];

        return new Record
        {
            Timestamp = DateTime.Parse(timestamp),
            Value = new Value(channel, JsonSerializer.Deserialize<string>(value)),
        };
    }
}

internal class Record
{
    public DateTime Timestamp { get; set; }
    public Value Value { get; set; }

    public override string ToString()
    {
        return $"Record @{Timestamp}: {Value}";
    }
}

internal class RecordOffset
{
    public RecordOffset(TimeSpan offset, Record @record)
    {
        Offset = offset;
        Record = record;
    }

    public TimeSpan Offset { get; }
    public Record Record { get; }
    
    public Value Value => Record.Value;
}
