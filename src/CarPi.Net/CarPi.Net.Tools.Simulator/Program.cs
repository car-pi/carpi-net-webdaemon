﻿using CarPi.Net.Core.ValueConversion;
using CarPi.Net.Core.ValueSubmitter;
using CarPi.Net.Redis;
using CarPi.Net.Tools.Simulator;
using CarPi.Net.Tools.Simulator.Arguments;
using CarPi.Net.Tools.Simulator.Reader;
using CommandLine;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using StackExchange.Redis;

Parser.Default.ParseArguments<SimulatorArguments>(args)
    .MapResult(opt =>
        {
            Host.CreateDefaultBuilder(args)
                .UseSerilog((_, lc) => lc
                    .WriteTo.Console()
                    .MinimumLevel.Debug())
                .ConfigureServices((_, services) =>
                {
                    services
                        .AddSingleton(opt)
                        .AddSingleton<FileReader>()
                        .AddSingleton<ConfigurationOptions>(new ConfigurationOptions
                        {
                            EndPoints = { opt.RedisAddress }
                        })
                        .AddSingleton<ConnectionMultiplexer>(r => ConnectionMultiplexer
                            .Connect(r.GetRequiredService<ConfigurationOptions>()))
                        .AddSingleton<IValueSubmitter, RedisValueSubmitter>()
                        .AddValueConversion()
                        .RegisterDefaultValueConverter()
                        .AddHostedService<SimulatorWorker>();
                })
                .Build()
                .Run();
            return 0;
        },
        _ => -1);
