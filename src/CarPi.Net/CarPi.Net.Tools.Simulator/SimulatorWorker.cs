using CarPi.Net.Core.ValueSubmitter;
using CarPi.Net.Tools.Simulator.Arguments;
using CarPi.Net.Tools.Simulator.Reader;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CarPi.Net.Tools.Simulator;

internal class SimulatorWorker : BackgroundService
{
    private readonly ILogger<SimulatorWorker> _logger;
    private readonly SimulatorArguments _args;
    private readonly FileReader _fileReader;
    private readonly IValueSubmitter _valueSubmitter;
    private readonly IHostApplicationLifetime _applicationLifetime;

    public SimulatorWorker(
        ILogger<SimulatorWorker> logger,
        SimulatorArguments args,
        FileReader fileReader,
        IValueSubmitter valueSubmitter,
        IHostApplicationLifetime applicationLifetime)
    {
        _logger = logger;
        _args = args;
        _fileReader = fileReader;
        _valueSubmitter = valueSubmitter;
        _applicationLifetime = applicationLifetime;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        List<string> filePaths = _args.InputFiles.ToList();
        _logger.LogInformation("Loading {FileCount} files ...", filePaths.Count);
        List<Record> records = filePaths
            .SelectMany(f =>
            {
                _logger.LogDebug("Reading file {FilePath} ...", f);
                return _fileReader.ReadRecordsFromFile(f);
            })
            .OrderBy(r => r.Timestamp)
            .ToList();
        _logger.LogInformation("Read {Count} records from {FileCount} file(s)",
            records.Count, filePaths.Count);

        _logger.LogInformation("Reorganizing records ...");
        DateTime firstRecord = records.Min(r => r.Timestamp);
        List<RecordOffset> organizedRecords = records
            .Select(r => GenerateRecordOffset(r, firstRecord))
            .ToList();

        _logger.LogInformation("Warming up Simulator ...");

        while (!stoppingToken.IsCancellationRequested)
        {
            bool stopped = await RunLoop(organizedRecords, stoppingToken);
            if (!_args.Loop || stopped)
            {
                break;
            }
        }

        _applicationLifetime.StopApplication();
    }

    private async Task<bool> RunLoop(
        List<RecordOffset> organizedRecords,
        CancellationToken cancellationToken)
    {
        var stopped = false;
        TimeSpan defaultInterval = TimeSpan.FromMilliseconds(_args.Interval);
        TimeSpan maxInterval = TimeSpan.FromMilliseconds(_args.MaxInterval);

        int index = 0,
            recordsCount = organizedRecords.Count;
        RecordOffset? prevRecord = null;
        foreach (RecordOffset @record in organizedRecords)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                stopped = true;
                break;
            }

            index++;
            if (prevRecord != null)
            {
                TimeSpan diff;
                switch (_args.Mode)
                {
                    case SimulatorMode.Realtime:
                        diff = record.Offset - prevRecord.Offset;
                        break;
                    case SimulatorMode.Constant:
                        diff = defaultInterval;
                        break;
                    default:
                        throw new NotImplementedException($"Mode {_args.Mode} not implemented");
                }

                if (diff > maxInterval)
                {
                    diff = maxInterval;
                }

                _logger.LogTrace("Sleeping for {TimeDiff} ...", diff);
                await Task.Delay(diff, cancellationToken);
            }

            _logger.LogDebug("Submitting {RecordIndex}/{RecordCount} {Record}", index, recordsCount, record.Record);
            _valueSubmitter.SubmitValue(record.Value.Channel, record.Value.StringValue);
            prevRecord = record;
        }

        return stopped;
    }

    public override async Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Shutting down simulator ...");
        await base.StopAsync(cancellationToken);
    }

    private RecordOffset GenerateRecordOffset(Record record, DateTime firstRecordTimestamp)
    {
        return new RecordOffset(record.Timestamp - firstRecordTimestamp, record);
    }
}