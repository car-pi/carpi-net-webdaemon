﻿using CarPi.Net.Core.ValueConversion;
using CarPi.Net.Core.ValueSubmitter;

namespace CarPi.Net.Core.ValueTransformation;

public abstract class BaseValueTransformer : IValueTransformer
{
    protected readonly IValueSubmitter _valueSubmitter;
    protected readonly IValueConversionManager _conversionManager;

    protected BaseValueTransformer(
        IValueSubmitter valueSubmitter,
        IValueConversionManager conversionManager)
    {
        _valueSubmitter = valueSubmitter;
        _conversionManager = conversionManager;
    }

    public abstract Channel[] SubscribedChannels { get; }

    public abstract void ReceiveValue(Value value);
}