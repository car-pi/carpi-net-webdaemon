namespace CarPi.Net.Core.ValueTransformation;

public interface IValueTransformer
{
    Channel[] SubscribedChannels { get; }
    void ReceiveValue(Value value);
}