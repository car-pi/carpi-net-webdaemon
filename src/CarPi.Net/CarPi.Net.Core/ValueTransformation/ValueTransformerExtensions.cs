using Microsoft.Extensions.DependencyInjection;

namespace CarPi.Net.Core.ValueTransformation;

public static class ValueTransformerExtensions
{
    public static IServiceCollection AddValueTransformer<TTransformer>(this IServiceCollection services)
        where TTransformer : class, IValueTransformer
    {
        return services
            .AddSingleton<IValueTransformer, TTransformer>();
    }
}