using System;

namespace CarPi.Net.Core.ValueConversion;

public class IntegerValueConverter : BaseConverter<int>
{
    public override ValueType UsableForType => ValueType.Integer;

    public override int Convert(string stringValue)
    {
        try
        {
            return int.Parse(stringValue);
        }
        catch (FormatException)
        {
            return (int)float.Parse(stringValue);
        }
    }

    public override string ConvertBack(int input) => input.ToString();
}