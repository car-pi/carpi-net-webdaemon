using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace CarPi.Net.Core.ValueConversion;

public interface IValueConversionManager
{
    object ConvertValue(Value value);
    T ConvertValue<T>(Value value);

    bool TryConvertValue(Value value, out object output);
    bool TryConvertValue<T>(Value value, out T output);

    string ConvertBack(Channel channel, object parsedValue);
    string ConvertBack<T>(T parsedValue);
}

public class ValueConversionManager : IValueConversionManager
{
    private readonly IImmutableDictionary<ValueType, IValueConverter> _valueConverters;
    private readonly IImmutableDictionary<Type, List<IValueConverter>> _typeLookup;

    public static IValueConversionManager Default => new ValueConversionManager(
        new List<IValueConverter>
        {
            new StringValueConverter(),
            new IntegerValueConverter(),
            new FloatValueConverter(),
            new BooleanValueConverter()
        });

    public ValueConversionManager(IEnumerable<IValueConverter> valueConverters)
    {
        List<IValueConverter> allValueConverters = valueConverters.ToList();
        _valueConverters = allValueConverters
            .ToImmutableDictionary(
                c => c.UsableForType,
                c => c);
        _typeLookup = allValueConverters
            .SelectMany(c => c.SupportedTypes.Select(t => new
            {
                Converter = c,
                Type = t
            }))
            .GroupBy(g => g.Type)
            .ToImmutableDictionary(
                g => g.Key,
                g => g.Select(i => i.Converter).ToList());
    }

    private IValueConverter GetValueConverter(ValueType valueType)
    {
        return _valueConverters[valueType];
    }

    private IValueConverter<T> GetValueConverter<T>()
    {
        return _typeLookup[typeof(T)].First() as IValueConverter<T>;
    }

    public object ConvertValue(Value value)
    {
        return GetValueConverter(value.Channel.Type)
            .Convert(value);
    }

    public T ConvertValue<T>(Value value)
    {
        return GetValueConverter<T>()
            .Convert(value);
    }

    public bool TryConvertValue(Value value, out object output)
    {
        IValueConverter valueConverter;
        try
        {
            valueConverter = GetValueConverter(value.Channel.Type);
        }
        catch (KeyNotFoundException)
        {
            output = null;
            return false;
        }

        return valueConverter.TryConvert(value.StringValue, out output);
    }

    public bool TryConvertValue<T>(Value value, out T output)
    {
        IValueConverter<T> valueConverter;
        try
        {
            valueConverter = GetValueConverter<T>();
        }
        catch (Exception ex) when (ex is KeyNotFoundException or InvalidOperationException)
        {
            output = default;
            return false;
        }

        return valueConverter.TryConvert(value.StringValue, out output);
    }

    public string ConvertBack(Channel channel, object parsedValue)
    {
        return GetValueConverter(channel.Type)
            .ConvertBack(parsedValue);
    }

    public string ConvertBack<T>(T parsedValue)
    {
        return GetValueConverter<T>()
            .ConvertBack(parsedValue);
    }
}