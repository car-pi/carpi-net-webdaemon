using System;
using System.Collections.Generic;

namespace CarPi.Net.Core.ValueConversion;

public abstract class BaseConverter<T> : IValueConverter<T>
{
    public abstract ValueType UsableForType { get; }
    public IList<Type> SupportedTypes => new List<Type> { typeof(T) };

    public abstract T Convert(string stringValue);

    object IValueConverter.Convert(string stringValue)
    {
        return Convert(stringValue);
    }

    public virtual bool TryConvert(string stringValue, out object output)
    {
        try
        {
            output = Convert(stringValue);
            return true;
        }
        catch
        {
            output = null;
            return false;
        }
    }

    public virtual bool TryConvert(string stringValue, out T output)
    {
        try
        {
            output = Convert(stringValue);
            return true;
        }
        catch
        {
            output = default;
            return false;
        }
    }

    public abstract string ConvertBack(T input);

    public virtual string ConvertBack(object input) => ConvertBack((T)input);
}