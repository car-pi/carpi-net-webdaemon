using System;
using System.Collections.Generic;

namespace CarPi.Net.Core.ValueConversion;

public interface IValueConverter
{
    ValueType UsableForType { get; }
    IList<Type> SupportedTypes { get; }
    object Convert(string stringValue);
    bool TryConvert(string stringValue, out object output);
    string ConvertBack(object input);
}

public interface IValueConverter<T> : IValueConverter
{
    new T Convert(string stringValue);
    bool TryConvert(string stringValue, out T output);

    string ConvertBack(T input);
}