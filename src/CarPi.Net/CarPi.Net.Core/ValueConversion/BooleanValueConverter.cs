namespace CarPi.Net.Core.ValueConversion;

public class BooleanValueConverter : BaseConverter<bool>
{
    public override ValueType UsableForType => ValueType.Boolean;
    public override bool Convert(string stringValue) => stringValue == "1";

    public override string ConvertBack(bool input) => input ? "1" : "0";
}