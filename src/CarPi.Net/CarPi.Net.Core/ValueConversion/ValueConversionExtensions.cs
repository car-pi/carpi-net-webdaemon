using Microsoft.Extensions.DependencyInjection;

namespace CarPi.Net.Core.ValueConversion;

public static class ValueConversionExtensions
{
    public static object Convert(this IValueConverter valueConverter, Value value)
        => valueConverter.Convert(value.StringValue);
    
    public static T Convert<T>(this IValueConverter<T> valueConverter, Value value)
        => valueConverter.Convert(value.StringValue);

    public static IServiceCollection AddValueConversion(this IServiceCollection services)
        => services.AddSingleton<IValueConversionManager, ValueConversionManager>();
    
    public static IServiceCollection RegisterValueConverter<T>(this IServiceCollection services)
        where T : class, IValueConverter
        => services.AddSingleton<IValueConverter, T>();

    public static IServiceCollection RegisterDefaultValueConverter(this IServiceCollection services)
        => services
            .RegisterValueConverter<StringValueConverter>()
            .RegisterValueConverter<IntegerValueConverter>()
            .RegisterValueConverter<FloatValueConverter>()
            .RegisterValueConverter<BooleanValueConverter>();
}