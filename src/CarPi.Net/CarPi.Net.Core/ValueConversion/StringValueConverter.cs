namespace CarPi.Net.Core.ValueConversion;

public class StringValueConverter : BaseConverter<string>
{
    public override ValueType UsableForType => ValueType.String;
    public override string Convert(string stringValue) => stringValue;
    public override string ConvertBack(string input) => $"{input}";
}