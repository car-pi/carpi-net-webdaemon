namespace CarPi.Net.Core.ValueConversion;

public class FloatValueConverter : BaseConverter<float>
{
    public override ValueType UsableForType => ValueType.Float;
    public override float Convert(string stringValue) => float.Parse(stringValue);
    public override string ConvertBack(float input) => input.ToString();

    public override bool TryConvert(string stringValue, out float output)
    {
        return float.TryParse(stringValue, out output);
    }
}