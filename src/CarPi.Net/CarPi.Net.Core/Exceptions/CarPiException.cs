using System;
using System.Runtime.Serialization;

namespace CarPi.Net.Core.Exceptions;

public class CarPiException : Exception
{
    public CarPiException()
    {
    }

    protected CarPiException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    public CarPiException(string message) : base(message)
    {
    }

    public CarPiException(string message, Exception innerException) : base(message, innerException)
    {
    }
}