using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace CarPi.Net.Core;

public class Channel
{
    private static readonly Regex ParseRegex = new("^([a-z]*)#(.*)$");

    public Channel(string name)
    {
        MatchCollection parsed = ParseRegex.Matches(name);
        if (!parsed.Any())
        {
            throw new ArgumentException("Provided channel name is invalid", nameof(name));
        }

        GroupCollection groups = parsed.First().Groups;
        Name = groups[2].Value;
        Type = groups[1].Value.ToValueType();
    }

    public Channel(string name, ValueType type)
    {
        Name = name;
        Type = type;
    }

    public Channel(ValueType type, string prefix, string key) : this($"{prefix}{key}", type)
    {
    }

    public string Name { get; }
    public ValueType Type { get; }

    public bool IsChannel(string channel)
    {
        return channel == Name;
    }

    public override string ToString()
    {
        return $"{Type.ToStringRepresentation()}#{Name}";
    }

    public static implicit operator string(Channel channel) => channel.ToString();
    public static implicit operator Channel(string channelStr) => new(channelStr);

    public static bool operator ==(Channel a, Channel b)
    {
        return a?.Equals(b) ?? false;
    }

    public static bool operator !=(Channel a, Channel b)
    {
        return !(a == b);
    }

    private bool Equals(Channel other)
    {
        return Name == other.Name && Type == other.Type;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((Channel)obj);
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Name, (int)Type);
    }

    private sealed class NameTypeEqualityComparer : IEqualityComparer<Channel>
    {
        public bool Equals(Channel x, Channel y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.Name == y.Name && x.Type == y.Type;
        }

        public int GetHashCode(Channel obj)
        {
            return HashCode.Combine(obj.Name, (int)obj.Type);
        }
    }

    public static IEqualityComparer<Channel> NameTypeComparer { get; } = new NameTypeEqualityComparer();
}

public static class ChannelExtensions
{
    public static Channel StringChannel(this string channel) => new(channel, ValueType.String);
    public static Channel IntChannel(this string channel) => new(channel, ValueType.Integer);
    public static Channel FloatChannel(this string channel) => new(channel, ValueType.Float);
    public static Channel BoolChannel(this string channel) => new(channel, ValueType.Boolean);
}