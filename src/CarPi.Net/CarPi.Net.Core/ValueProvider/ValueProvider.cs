using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using CarPi.Net.Core.ValueConversion;
using CarPi.Net.Core.ValueListener;

namespace CarPi.Net.Core.ValueProvider;

public class ValueProvider : IDisposable
{
    protected readonly IValueListener _listener;
    protected readonly IValueConversionManager _conversionManager;

    protected readonly ConcurrentDictionary<string, Value> _currentValues;

    public ValueProvider(IValueListener listener, IValueConversionManager conversionManager)
    {
        _listener = listener;
        _conversionManager = conversionManager;
        _currentValues = new ConcurrentDictionary<string, Value>();

        _listener.NewValueReceived += OnNewValueReceived;
    }

    private void OnNewValueReceived(Value value)
    {
        try
        {
            _currentValues[value.Channel] = value;
        }
        catch (Exception ex)
        {
            var x = 1;
        }
    }

    private Value GetValue(string channel) => _currentValues.GetValueOrDefault(channel);

    public string GetRaw(string channel) => GetValue(channel)?.StringValue;

    public T Get<T>(string channel) => _conversionManager.ConvertValue<T>(GetValue(channel));

    public T Get<T>(string channel, T defaultValue) => TryGetValue(channel, out T value) ? value : defaultValue;

    public string Get(string channel, string defaultValue) =>
        TryGetValue(channel.StringChannel(), out string value) ? value : defaultValue;

    public int Get(string channel, int defaultValue) =>
        TryGetValue(channel.IntChannel(), out int value) ? value : defaultValue;

    public bool Get(string channel, bool defaultValue) =>
        TryGetValue(channel.BoolChannel(), out bool value) ? value : defaultValue;

    public float Get(string channel, float defaultValue) =>
        TryGetValue(channel.FloatChannel(), out float value) ? value : defaultValue;

    public bool TryGetValue<T>(string channel, out T value)
    {
        if (_currentValues.TryGetValue(channel, out Value v))
        {
            return _conversionManager.TryConvertValue(v, out value);
        }

        value = default;
        return false;
    }

    public Dictionary<string, object> GetValues() => _currentValues
        .Where(kvp => kvp.Value != null)
        .Select(kvp =>
        {
            (string key, Value value) = kvp;
            object convertedValue = null;
            _conversionManager.TryConvertValue(value, out convertedValue);
            return new
            {
                Key = key,
                Value = convertedValue
            };
        })
        //.Where(i => i != null)
        .ToDictionary(
            i => i.Key,
            i => i.Value);

    public void Dispose()
    {
        _listener.NewValueReceived -= OnNewValueReceived;
    }
}