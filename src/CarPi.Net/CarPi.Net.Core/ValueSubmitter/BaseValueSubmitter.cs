﻿using CarPi.Net.Core.ValueConversion;

namespace CarPi.Net.Core.ValueSubmitter;

public abstract class BaseValueSubmitter : IValueSubmitter
{
    protected readonly IValueConversionManager _conversionManager;
    protected BaseValueSubmitter(IValueConversionManager conversionManager)
    {
        _conversionManager = conversionManager;
    }

    public void SubmitValue(Channel channel, string value) => Submit(channel, value);

    public void SubmitValue<T>(Channel channel, T value)
    {
        Submit(channel, _conversionManager.ConvertBack(value));
    }

    protected abstract void Submit(string channel, string value);
}
