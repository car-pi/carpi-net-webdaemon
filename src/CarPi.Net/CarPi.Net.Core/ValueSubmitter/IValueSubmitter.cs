﻿namespace CarPi.Net.Core.ValueSubmitter;

public interface IValueSubmitter
{
    void SubmitValue(Channel channel, string value);
    void SubmitValue<T>(Channel channel, T value);
}
