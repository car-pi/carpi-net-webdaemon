using System.Collections.Generic;
using System.Linq;

namespace CarPi.Net.Core;

public enum ValueType
{
    String,
    Float,
    Integer,
    Boolean,
}

public static class ValueTypeExtensions
{
    private static readonly IReadOnlyDictionary<string, ValueType> ValueTypes = new Dictionary<string, ValueType>
    {
        { "s", ValueType.String },
        { "i", ValueType.Integer },
        { "f", ValueType.Float },
        { "b", ValueType.Boolean }
    };

    public static string ToStringRepresentation(this ValueType valueType)
    {
        return ValueTypes
            .Where(kvp => kvp.Value == valueType)
            .Select(kvp => kvp.Key)
            .FirstOrDefault();
    }

    public static ValueType ToValueType(this string str) => ValueTypes[str];
}