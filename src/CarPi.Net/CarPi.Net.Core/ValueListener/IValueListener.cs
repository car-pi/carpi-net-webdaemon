using System;

namespace CarPi.Net.Core.ValueListener;

public delegate void NewValueReceivedDelegate(Value value);
public delegate void MessageParsingFailedDelegate(string channel, string message, Exception ex);

public delegate void
    MessageHandlerFailedDelegate(Value value, NewValueReceivedDelegate offendingDelegate, Exception ex);

public interface IValueListener
{
    event MessageParsingFailedDelegate MessageParsingFailed;
    event MessageHandlerFailedDelegate MessageHandlerFailed;
    event NewValueReceivedDelegate NewValueReceived;

    void StartListening();
    void StopListening();
    
    void RegisterChannelHandler(string channel, NewValueReceivedDelegate handler);
}