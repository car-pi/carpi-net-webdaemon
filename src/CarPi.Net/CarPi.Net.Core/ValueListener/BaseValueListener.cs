using System;
using System.Collections.Generic;

namespace CarPi.Net.Core.ValueListener;

public abstract class BaseValueListener : IValueListener, IDisposable
{
    private readonly Dictionary<string, List<NewValueReceivedDelegate>> _eventHandlers;

    protected BaseValueListener()
    {
        _eventHandlers = new Dictionary<string, List<NewValueReceivedDelegate>>();
    }

    public event MessageParsingFailedDelegate MessageParsingFailed;
    public event MessageHandlerFailedDelegate MessageHandlerFailed;
    public event NewValueReceivedDelegate NewValueReceived;

    public abstract void StartListening();
    public abstract void StopListening();

    public virtual void RegisterChannelHandler(string channel, NewValueReceivedDelegate handler)
    {
        if (!_eventHandlers.ContainsKey(channel))
        {
            _eventHandlers.Add(channel, new List<NewValueReceivedDelegate>());
        }
        _eventHandlers[channel].Add(handler);
    }

    protected void HandleNewValue(string channel, string message)
    {
        Value value;
        try
        {
            value = new Value(channel, message);
        }
        catch (Exception ex)
        {
            MessageParsingFailed?.Invoke(channel, message, ex);
            return;
        }

        try
        {
            NewValueReceived?.Invoke(value);
        } catch { /* ignored */ }

        if (_eventHandlers.ContainsKey(channel))
        {
            foreach (NewValueReceivedDelegate handler in _eventHandlers[channel])
            {
                try
                {
                    handler(value);
                }
                catch (Exception ex)
                {
                    MessageHandlerFailed?.Invoke(value, handler, ex);
                }
            }
        }
    }

    public virtual void Dispose()
    {
        _eventHandlers.Clear();
    }
}