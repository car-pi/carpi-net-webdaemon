using CarPi.Net.Core.ValueConversion;

namespace CarPi.Net.Core.ValueListener;

public delegate void NewTypedValueReceivedDelegate<in T>(Value value, T parsedValue);

public static class ValueListenerExtensions
{
    public static void RegisterChannelHandler(
        this IValueListener valueListener,
        Channel channel,
        NewValueReceivedDelegate handler)
    {
        valueListener.RegisterChannelHandler(
            channel.ToString(),
            handler);
    }

    public static void RegisterChannelHandler(
        this IValueListener valueListener,
        ValueType valueType,
        string channelName,
        NewValueReceivedDelegate handler)
    {
        valueListener.RegisterChannelHandler(
            new Channel(channelName, valueType),
            handler);
    }

    public static void RegisterChannelHandler<T>(
        this IValueListener valueListener,
        IValueConversionManager converter,
        Channel channel,
        NewTypedValueReceivedDelegate<T> handler)
    {
        valueListener.RegisterChannelHandler(
            channel,
            value =>
            {
                if (converter.TryConvertValue(value, out T convertedValue))
                {
                    handler.Invoke(value, convertedValue);
                }
            });
    }

    public static void RegisterChannelHandler<T>(
        this IValueListener valueListener,
        IValueConversionManager converter,
        ValueType valueType,
        string channelName,
        NewTypedValueReceivedDelegate<T> handler)
    {
        valueListener.RegisterChannelHandler(
            new Channel(channelName, valueType),
            value =>
            {
                if (converter.TryConvertValue(value, out T convertedValue))
                {
                    handler.Invoke(value, convertedValue);
                }
            });
    }
}