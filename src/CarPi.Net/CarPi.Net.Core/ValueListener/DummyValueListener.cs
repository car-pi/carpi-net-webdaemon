using CarPi.Net.Core.ValueConversion;

namespace CarPi.Net.Core.ValueListener;

public class DummyValueListener : BaseValueListener
{
    public DummyValueListener(IValueConversionManager valueConversionManager) : base()
    {
    }

    public override void StartListening()
    {
    }

    public override void StopListening()
    {
    }

    public void SubmitValue(string channel, string message)
    {
        HandleNewValue(channel, message);
    }
}