namespace CarPi.Net.Core;

public class Value
{
    public Value()
    {
    }

    public Value(string channel, string stringValue)
    {
        Channel = channel;
        StringValue = stringValue;
    }

    public Value(Channel channel, string stringValue)
    {
        Channel = channel;
        StringValue = stringValue;
    }

    public Channel Channel { get; }
    public string StringValue { get; }

    public bool IsOfType(ValueType valueType) => Channel.Type == valueType;

    public override string ToString()
    {
        return $"{nameof(Channel)}: {Channel}, {nameof(StringValue)}: {StringValue}";
    }
}