using System.Collections.Generic;

namespace CarPi.Net.Core.Command;

public interface ICommandInvoker
{
    public void InvokeCommand(string command);
    public void InvokeCommand(string command, Dictionary<string, object> parameters);
}