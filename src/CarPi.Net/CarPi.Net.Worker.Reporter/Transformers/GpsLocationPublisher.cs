using System.Collections.Immutable;
using System.Diagnostics;
using System.Reflection;
using System.Text.Json;
using CarPi.Net.Core;
using CarPi.Net.Core.Keys;
using CarPi.Net.Core.ValueConversion;
using CarPi.Net.Core.ValueSubmitter;
using CarPi.Net.Core.ValueTransformation;
using CarPi.Net.Worker.Reporter.Output.PhoneTrack;
using Geolocation;
using Microsoft.Extensions.Options;

namespace CarPi.Net.Worker.Reporter.Transformers;

public class GpsLocationPublisher : BaseValueTransformer
{
    private static readonly IDictionary<Channel, Action<PhoneTrackObject, Value, IValueConversionManager>>
        PropertyMappings =
            new Dictionary<Channel, Action<PhoneTrackObject, Value, IValueConversionManager>>
            {
                {
                    GpsChannels.Latitude,
                    (p, v, c) => p.Latitude = c.ConvertValue<float>(v)
                },
                {
                    GpsChannels.Longitude,
                    (p, v, c) => p.Longitude = c.ConvertValue<float>(v)
                },
                {
                    GpsChannels.Altitude,
                    (p, v, c) => p.Altitude = c.ConvertValue<float>(v)
                },
                {
                    GpsChannels.Timestamp,
                    (p, v, _) => p.Timestamp = DateTimeOffset.Parse(v.StringValue).ToUnixTimeSeconds()
                },
                {
                    ObdChannels.BatterySoC,
                    (p, v, c) => p.Battery = c.ConvertValue<float>(v)
                },
                {
                    GpsChannels.Speed,
                    (p, v, c) => p.Speed = c.ConvertValue<float>(v)
                },
                {
                    GpsChannels.Track,
                    (p, v, c) => p.Bearing = c.ConvertValue<float>(v)
                },
                {
                    GpsChannels.LatitudeError,
                    (p, v, c) => p.LatError = c.ConvertValue<float>(v)
                },
                {
                    GpsChannels.LongitudeError,
                    (p, v, c) => p.LonError = c.ConvertValue<float>(v)
                },
                {
                    GpsChannels.FixMode,
                    (p, v, c) => p.FixMode = c.ConvertValue<int>(v)
                }
            }.ToImmutableDictionary();

    private static readonly string AppVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location)
        .ProductVersion ?? "unknown";

    private readonly ILogger<GpsLocationPublisher> _logger;
    private readonly IPhoneTrackOutput _output;

    private readonly PhoneTrackObject _phoneTrackObject = new()
    {
        UserAgent = $"CarPiNetReporter/{AppVersion}"
    };

    private readonly PhoneTrackConfiguration _config;
    private DateTime _lastSendAttempt = DateTime.UtcNow;
    private readonly TimeSpan _interval;
    private readonly TimeSpan _forceResendInterval;

    private PhoneTrackObject? _lastReportedLocation = null;

    public GpsLocationPublisher(
        IValueSubmitter valueSubmitter,
        IValueConversionManager conversionManager,
        ILogger<GpsLocationPublisher> logger,
        IPhoneTrackOutput output,
        IOptions<PhoneTrackConfiguration> config)
        : base(valueSubmitter, conversionManager)
    {
        _logger = logger;
        _output = output;
        _config = config.Value;
        _interval = TimeSpan.FromMilliseconds(_config.Timeout);
        _forceResendInterval = TimeSpan.FromSeconds(_config.ForceResendInterval);
    }

    public override Channel[] SubscribedChannels => PropertyMappings.Keys.ToArray();

    public override void ReceiveValue(Value value)
    {
        lock (_phoneTrackObject)
        {
            if (PropertyMappings.ContainsKey(value.Channel))
            {
                try
                {
                    PropertyMappings[value.Channel](_phoneTrackObject, value, _conversionManager);
                }
                catch (FormatException ex)
                {
                    _logger.LogInformation("Could not convert value {Value} from {Channel}: {ExceptionMessage}",
                        value.StringValue, value.Channel, ex.Message);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "Failed to convert value from {Channel} due to an exception",
                        value.Channel);
                }
            }

            if (CanBeSubmitted(_phoneTrackObject))
            {
                if (_output.Submit(_phoneTrackObject))
                {
                    _lastSendAttempt = DateTime.UtcNow;
                    _lastReportedLocation = _phoneTrackObject.Clone();
                    _valueSubmitter.SubmitValue(ReporterChannels.LastSubmittedTimeStamp,
                        $"{_lastSendAttempt:yyyy-MM-dd'T'HH:mm:ss'Z'}");
                    _valueSubmitter.SubmitValue(ReporterChannels.LastSubmittedValue,
                        JsonSerializer.Serialize(_lastReportedLocation));
                } else
                {
                    _lastSendAttempt = DateTime.UtcNow;
                }
            }
        }
    }

    private bool CanBeSubmitted(PhoneTrackObject obj)
    {
        bool passedPreChecks = obj.Latitude != 0f
                               && obj.Longitude != 0f
                               && obj.Timestamp != 0
                               && DateTime.UtcNow - _lastSendAttempt > _interval;
        if (!passedPreChecks)
        {
            return false;
        }

        bool passedAccuracyCheck = obj.FixMode >= 2
                                   && (obj.LatError <= _config.MinAccuracy || obj.LonError <= _config.MinAccuracy)
                                   && (obj.LatError > 0 && obj.LonError > 0);
        if (!passedAccuracyCheck)
        {
            _logger.LogDebug(
                "Will not report current location because fix was not accurate enough (Fix Mode={FixMode} (min {MinFixMode}), Accuracy={LatError}m,{LonError}m (max {MaxError}))",
                obj.FixMode, 2, obj.LatError, obj.LonError, _config.MinAccuracy);
        }

        var passedDistanceCheck = true;
        if (_lastReportedLocation != null && _config.MinDistance > 0)
        {
            double distance = CalculateDistance(obj, _lastReportedLocation);
            passedDistanceCheck = distance > _config.MinDistance;
            if (!passedDistanceCheck)
            {
                _logger.LogDebug(
                    "Will not report current location because current location is only {DistanceCalculated}m away from last reported location. Min. distance configured is {MinDistanceCalculated}m",
                    distance,
                    _config.MinDistance);
            }
        }

        bool forceSend = _forceResendInterval.TotalSeconds > 0 && DateTime.UtcNow - _lastSendAttempt > _forceResendInterval;
        return passedPreChecks
               && passedAccuracyCheck
               && (passedDistanceCheck || forceSend);
    }

    private static double CalculateDistance(
        PhoneTrackObject a,
        PhoneTrackObject b)
    {
        return GeoCalculator.GetDistance(
            a.Latitude,
            a.Longitude,
            b.Latitude,
            b.Longitude,
            1,
            DistanceUnit.Meters);
    }
}