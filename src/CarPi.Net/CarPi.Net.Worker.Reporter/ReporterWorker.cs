using CarPi.Net.Core;
using CarPi.Net.Core.ValueListener;
using CarPi.Net.Core.ValueTransformation;

namespace CarPi.Net.Worker.Reporter;

public class ReporterWorker : BackgroundService
{
    private readonly ILogger<ReporterWorker> _logger;
    private readonly IValueTransformer[] _valueTransformers;
    private readonly IValueListener _valueListener;

    public ReporterWorker(
        ILogger<ReporterWorker> logger,
        IEnumerable<IValueTransformer> valueTransformers,
        IValueListener valueListener)
    {
        _logger = logger;
        _valueListener = valueListener;
        _valueTransformers = valueTransformers.ToArray();
    }

    public override Task StartAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Starting service ...");
        _valueListener.StartListening();

        foreach (IValueTransformer transformer in _valueTransformers)
        {
            foreach (Channel? channel in transformer.SubscribedChannels)
            {
                _valueListener.RegisterChannelHandler(channel, transformer.ReceiveValue);
            }
        }

        return base.StartAsync(cancellationToken);
    }

    public override Task StopAsync(CancellationToken cancellationToken)
    {
        _logger.LogInformation("Stopping service ...");
        _valueListener.StopListening();
        return base.StopAsync(cancellationToken);
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            await Task.Delay(1000, stoppingToken);
        }
    }
}