namespace CarPi.Net.Worker.Reporter.Output.PhoneTrack;

public class PhoneTrackConfiguration
{
    public string BaseUrl { get; set; }
    public string SessionToken { get; set; }
    public string DeviceName { get; set; }
    public int Timeout { get; set; } = 15000;
    public double MinDistance { get; set; }
    public float MinAccuracy { get; set; }
    public int ForceResendInterval { get; set; }
}