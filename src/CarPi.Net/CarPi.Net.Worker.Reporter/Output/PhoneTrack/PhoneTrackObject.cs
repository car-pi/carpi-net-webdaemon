using System.Text.Json.Serialization;

namespace CarPi.Net.Worker.Reporter.Output.PhoneTrack;

public class PhoneTrackObject
{
    [JsonPropertyName("lat")] public float Latitude { get; set; }
    [JsonPropertyName("lon")] public float Longitude { get; set; }
    [JsonPropertyName("alt")] public float Altitude { get; set; }
    [JsonPropertyName("timestamp")] public long Timestamp { get; set; }
    [JsonPropertyName("bat")] public float Battery { get; set; }
    [JsonPropertyName("useragent")] public string UserAgent { get; set; }
    [JsonPropertyName("speed")] public float Speed { get; set; }
    [JsonPropertyName("bearing")] public float Bearing { get; set; }

    [JsonIgnore] public float LonError { get; set; } = float.MaxValue;
    [JsonIgnore] public float LatError { get; set; } = float.MaxValue;
    [JsonIgnore] public int FixMode { get; set; }

    public PhoneTrackObject Clone()
    {
        return new PhoneTrackObject
        {
            Latitude = Latitude,
            Longitude = Longitude,
            Altitude = Altitude,
            Timestamp = Timestamp,
            Battery = Battery,
            UserAgent = UserAgent,
            Speed = Speed,
            Bearing = Bearing,
            LonError = LonError,
            LatError = LatError,
            FixMode = FixMode,
        };
    }
}