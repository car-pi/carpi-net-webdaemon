using CarPi.Net.Core.Keys;
using CarPi.Net.Core.ValueSubmitter;
using Microsoft.Extensions.Options;
using RestSharp;

namespace CarPi.Net.Worker.Reporter.Output.PhoneTrack;

public class PhoneTrackOutput : IPhoneTrackOutput
{
    private readonly ILogger<PhoneTrackOutput> _logger;
    private readonly RestClient _client;
    private readonly PhoneTrackConfiguration _config;
    private readonly IValueSubmitter _valueSubmitter;

    public PhoneTrackOutput(
        ILogger<PhoneTrackOutput> logger,
        IOptions<PhoneTrackConfiguration> config,
        IValueSubmitter valueSubmitter)
    {
        _logger = logger;
        _valueSubmitter = valueSubmitter;
        _config = config.Value;
        _client = new RestClient(new RestClientOptions(_config.BaseUrl)
        {
            ThrowOnAnyError = false,
            Timeout = _config.Timeout
        });
    }

    public bool Submit(PhoneTrackObject obj)
    {
        RestRequest request = new RestRequest($"logPost/{_config.SessionToken}/{_config.DeviceName}", Method.Post)
            .AddJsonBody(obj);
        Uri uri = _client.BuildUri(request);
        _logger.LogDebug("Submitting values {@Obj} to PhoneTrack at {Uri}", obj, uri);

        try
        {
            RestResponse response = _client.PostAsync(request)
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult();
            if ((int)response.StatusCode >= 400)
            {
                _logger.LogWarning("Failed to submit values, Status {Status}", response.StatusCode);
                ReportFailure($"HTTP {(int)response.StatusCode} {response.StatusCode}");
                return false;
            }

            _logger.LogInformation("Submission completed, Status {Status}", response.StatusCode);
            return true;
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, "Failed to submit values due to an exception");
            ReportFailure($"Exception: {ex.Message}");
            return false;
        }
    }

    private void ReportFailure(string message)
    {
        _valueSubmitter.SubmitValue(ReporterChannels.LastFailureTimeStamp,
            $"{DateTime.UtcNow:yyyy-MM-dd'T'HH:mm:ss'Z'}");
        _valueSubmitter.SubmitValue(ReporterChannels.LastFailureMessage, message);
    }
}