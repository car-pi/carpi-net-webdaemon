namespace CarPi.Net.Worker.Reporter.Output.PhoneTrack;

public interface IPhoneTrackOutput
{
    bool Submit(PhoneTrackObject obj);
}