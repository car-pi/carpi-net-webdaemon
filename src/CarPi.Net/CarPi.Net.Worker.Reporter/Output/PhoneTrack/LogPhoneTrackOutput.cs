using System.Text.Json;

namespace CarPi.Net.Worker.Reporter.Output.PhoneTrack;

public class LogPhoneTrackOutput : IPhoneTrackOutput
{
    private readonly ILogger<LogPhoneTrackOutput> _logger;

    public LogPhoneTrackOutput(
        ILogger<LogPhoneTrackOutput> logger)
    {
        _logger = logger;
    }

    public bool Submit(PhoneTrackObject obj)
    {
        string json = JsonSerializer.Serialize(obj);
        _logger.LogInformation("Would submit JSON {Json}", json);
        return true;
    }
}