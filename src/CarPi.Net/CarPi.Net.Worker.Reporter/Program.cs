using CarPi.Net.Core.ValueConversion;
using CarPi.Net.Core.ValueProvider;
using CarPi.Net.Core.ValueTransformation;
using CarPi.Net.Redis;
using CarPi.Net.Worker.Reporter;
using CarPi.Net.Worker.Reporter.Output.PhoneTrack;
using CarPi.Net.Worker.Reporter.Transformers;
using Serilog;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureAppConfiguration(config =>
    {
#if DEBUG
        config.AddYamlFile("appsettings.yaml", optional: true);
#endif
        config.AddYamlFile("/etc/carpi/reporter.yaml", optional: true);
    })
    .UseSerilog((ctx, lc) => lc
        .ReadFrom.Configuration(ctx.Configuration))
    .ConfigureServices((ctx, services) =>
    {
        services
            .AddRedis(ctx.Configuration.GetConnectionString("Redis"))
            .AddSingleton<ValueProvider>()
            .AddValueConversion()
            .RegisterDefaultValueConverter()
            .AddSingleton<IPhoneTrackOutput, PhoneTrackOutput>()
            .AddValueTransformer<GpsLocationPublisher>()
            .Configure<PhoneTrackConfiguration>(o =>
                ctx.Configuration.GetSection("PhoneTrack").Bind(o))
            .AddHostedService<ReporterWorker>();
    })
    .Build();

await host.RunAsync();