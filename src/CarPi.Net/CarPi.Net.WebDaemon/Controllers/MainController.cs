using CarPi.Net.Core.ValueProvider;
using Microsoft.AspNetCore.Mvc;

namespace CarPi.Net.WebDaemon.Controllers;

[ApiController]
[Route("")]
public class MainController : ControllerBase
{
    private readonly ValueProvider _valueProvider;

    public MainController(ValueProvider valueProvider)
    {
        _valueProvider = valueProvider;
    }

    [HttpGet]
    public Dictionary<string, object> GetValues()
    {
        return _valueProvider.GetValues();
    }
}