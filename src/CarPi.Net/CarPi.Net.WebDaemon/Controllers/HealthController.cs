using Microsoft.AspNetCore.Mvc;

namespace CarPi.Net.WebDaemon.Controllers;

[ApiController]
[Route("health")]
public class HealthController : ControllerBase
{
    [HttpGet]
    public object GetHealth()
    {
        return new
        {
            Alive = true
        };
    }
}