using CarPi.Net.Core.Command;
using Microsoft.AspNetCore.Mvc;

namespace CarPi.Net.WebDaemon.Controllers;

[ApiController]
[Route("command")]
public class CommandController : ControllerBase
{
    private readonly ICommandInvoker _commandInvoker;

    public CommandController(ICommandInvoker commandInvoker)
    {
        _commandInvoker = commandInvoker;
    }

    [HttpPost("{commandName}")]
    public IActionResult SubmitCommand(
        [FromRoute] string commandName,
        [FromBody] Dictionary<string, object> obj)
    {
        _commandInvoker.InvokeCommand(commandName, obj);
        return NoContent();
    }
}