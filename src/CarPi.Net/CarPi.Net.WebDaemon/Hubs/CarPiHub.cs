using Microsoft.AspNetCore.SignalR;

namespace CarPi.Net.WebDaemon.Hubs;

public class CarPiHub : Hub
{
    private readonly ILogger<CarPiHub> _logger;
    private readonly Guid _instanceGuid;

    public CarPiHub(ILogger<CarPiHub> logger)
    {
        _instanceGuid = Guid.NewGuid();
        _logger = logger;
    }

    public DateTime Ping()
    {
        _logger.LogInformation("{Instance}: Ping", _instanceGuid);
        return DateTime.UtcNow;
    }

    public async void SubToValue(string valueKey)
    {
        _logger.LogInformation("Client {SrConnectionId} subscribed to {SrValueKey}", Context.ConnectionId, valueKey);
        await Groups.AddToGroupAsync(Context.ConnectionId, valueKey);
    }
}