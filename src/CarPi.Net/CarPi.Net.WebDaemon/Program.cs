using CarPi.Net.Core.Command;
using CarPi.Net.Core.ValueConversion;
using CarPi.Net.Core.ValueListener;
using CarPi.Net.Core.ValueProvider;
using CarPi.Net.Redis;
using CarPi.Net.WebDaemon;
using CarPi.Net.WebDaemon.Hubs;
using Serilog;
using StackExchange.Redis;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
builder.Host
    .ConfigureAppConfiguration(config => { config.AddYamlFile("/etc/carpi/webdaemon.yaml", optional: true); });
builder.Host.UseSerilog((ctx, lc) => lc
    .WriteTo.Console()
    .MinimumLevel.Debug());

// Add services to the container.

IServiceCollection services = builder.Services;
services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
services.AddEndpointsApiExplorer();
services.AddSwaggerGen();

services
    .AddCors(c => c.AddPolicy("Default", p => p
        .WithOrigins("http://localhost:4200")
        .AllowAnyMethod()
        .AllowAnyHeader()
        .AllowCredentials()))
    .AddSignalR();

services
    .AddSingleton<ConfigurationOptions>(r => new ConfigurationOptions
    {
        EndPoints =
        {
            builder.Configuration.GetConnectionString("Redis")
        }
    })
    .AddSingleton<ConnectionMultiplexer>(r => ConnectionMultiplexer
        .Connect(r.GetRequiredService<ConfigurationOptions>()))
    .AddSingleton<IValueListener, RedisValueListener>()
    .AddSingleton<ValueProvider>()
    .AddSingleton<ICommandInvoker, RedisCommandInvoker>()
    .AddSingleton<CarPi2SignalRTransmission>();

services
    .AddValueConversion()
    .RegisterDefaultValueConverter();

WebApplication app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("Default");

app.UseRouting();
app.UseAuthorization();
app.UseEndpoints(e =>
{
    e.MapControllers();
    e.MapHub<CarPiHub>("/carpi");
});

app.Services.GetRequiredService<IValueListener>()
    .StartListening();
app.Services.GetRequiredService<ValueProvider>();
app.Services.GetRequiredService<CarPi2SignalRTransmission>();

app.Run();