using CarPi.Net.Core;
using CarPi.Net.Core.ValueListener;
using CarPi.Net.WebDaemon.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace CarPi.Net.WebDaemon;

public class CarPi2SignalRTransmission
{
    private readonly ILogger<CarPi2SignalRTransmission> _logger;
    private readonly IHubContext<CarPiHub> _hubContext;
    private readonly IValueListener _valueListener;

    public CarPi2SignalRTransmission(ILogger<CarPi2SignalRTransmission> logger, IHubContext<CarPiHub> hubContext,
        IValueListener valueListener)
    {
        _logger = logger;
        _hubContext = hubContext;
        _valueListener = valueListener;

        _valueListener.NewValueReceived += ValueListenerOnNewValueReceived;

        _logger.LogInformation("Initialized");
    }

    private void ValueListenerOnNewValueReceived(Value value)
    {
        _logger.LogTrace("Value received {@Value}, transmitting over SignalR", value);
        //_hubContext.Clients.All.SendAsync("ValueReceived", value.Channel.ToString(), value.StringValue);
        _hubContext.Clients.Group(value.Channel)
            .SendAsync("ValueReceived", value.Channel.ToString(), value.StringValue);
    }
}